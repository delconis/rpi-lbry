FROM sdhibit/rpi-raspbian
MAINTAINER David Finch <delconisng@gmail.com>

RUN apt-get update && apt-get install -y sudo wget git libgmp3-dev build-essential python-dev python-pip
RUN git clone https://delconis@bitbucket.org/delconis/rpi-lbry.git
RUN git clone https://github.com/lbryio/lbrycrd
